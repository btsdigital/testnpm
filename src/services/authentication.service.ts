import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()
export class AuthenticationService {
    static hostname: string = "http://localhost";
    public userObj: any = {};
    constructor(private http: Http) { }

    login(username: string, password: string) {

        return this.http.post(AuthenticationService.hostname + '/PulseServices/Authenticate.svc/Login', { username: username, password: password })
            .map((response: Response) => {
                // login successful if there's a response cookie in the response
                let result = response.json();
                if (result.LoginResult && result.LoginResult.success) {
                    // store user details and cookies in local storage to keep user logged in between page refreshes
                    this.userObj = JSON.stringify(result.LoginResult);
                    localStorage.setItem('currentUser', this.userObj);

                }
                return result.LoginResult;
            });
    }
    getAuthenticationObject() {
        return JSON.parse(localStorage.getItem('currentUser'));
    }

    isAuthenticated(): boolean {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            return true;
        }
        else
        {
            return false;
        }
    }
    forgotPassword(email: string) {

        return this.http.post(AuthenticationService.hostname + '/PulseServices/Authenticate.svc/ForgotPassword', { email: email })
            .map((response: Response) => {
                // login successful if there's a response cookie in the response
                let result = response.json();
                if (result.ForgotPasswordResult && result.ForgotPasswordResult.success) {
                    //do nothing
                }
                return result.ForgotPasswordResult;
            });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}

export interface AuthenticationServiceConfig {
    hostname: string
}

export function ConfigureAuthenticationService(configuration: AuthenticationServiceConfig) {
    AuthenticationService.hostname = configuration.hostname;
}