import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService } from '../services/alert.service';
import { AuthenticationService } from '../services/authentication.service';

@Component({
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    showLoginPage: boolean = true;
    public projectName: string = 'Pulse';
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService
    ) { }

    ngOnInit() {
        //reset login status
        this.authenticationService.logout();

        //get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        var url = decodeURIComponent(window.location.href);
        var project = url.split('?returnUrl=')[1];

        if (typeof project == 'undefined' || project.length < 1 || project == '/' || project == '') {
            this.projectName = 'Pulse';
        }
        else {
            var result = project.replace('/', '').replace(/([A-Z])/g, " $1");
            var finalResult = result.charAt(0).toUpperCase() + result.slice(1);
            this.projectName = finalResult;
        }
    }

    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
            data => {
                if (data && data.success) {
                    this.router.navigate([this.returnUrl]);
                }
                else {
                    this.alertService.error(data.errorMessage);
                    this.loading = false;
                }
            },
            error => {
                console.log(error);
                this.alertService.error("Invalid Username / password");
                this.loading = false;
            });
    }

    showForgotPassword() {
        //this.router.navigate(['/forgotPassword']);
        this.showLoginPage = false;
    }

    forgotPassword() {
        this.loading = true;
        this.authenticationService.forgotPassword(this.model.username)
            .subscribe(
            data => {
                if (data && data.success) {
                    this.alertService.success(data.errorMessage);
                }
                else {
                    this.alertService.error(data.errorMessage);
                    this.loading = false;
                }
            },
            error => {
                console.log(error);
                this.alertService.error("Invalid Email");
                this.loading = false;
            });
    }
    cancelForgotPassword() {
        this.showLoginPage = true;
    }
}