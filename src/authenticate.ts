import { NgModule } from '@angular/core';

import { LoginComponent } from './components/login.component';
import { AuthGuard } from './guard/auth.guard';
import { AlertComponent } from './directives/alert.component';
import { AlertService } from './services/alert.service';
import { AuthenticationService, ConfigureAuthenticationService, AuthenticationServiceConfig } from './services/authentication.service';

@NgModule({
    imports: [

    ],
    exports: [
        LoginComponent,
        AuthGuard,
        AlertComponent,
        AlertService,
        AuthenticationService
    ],
    declarations: [
        LoginComponent,
        AlertComponent
    ],
    providers: [
        AlertService,
        AuthenticationService,
        AuthGuard
    ],
})
export class PulseAuthenticationLibrary { }
export { ConfigureAuthenticationService, AuthenticationServiceConfig }